import sys

if __name__ == '__main__':
    sys.path.append('src')
    sys.path.append('models')
from SuperEnv import SuperEnv
from ddpg import DDPG
from Defines import *
from config import *
from models import *
from noise import OrnsteinUhlenbeckProcess
import numpy as np

import tensorflow as tf


def player_maker(Sess, team, index, env):

    observation_shape = env.observation_space.spaces[team].shape

    action_shape = env.action_space.spaces[team].shape
    num_actions = action_shape[0]
    action_range = (env.action_space.spaces[team].low, env.action_space.spaces[team].high)

    # attack or move in the simplest setting.
    action_noise = OrnsteinUhlenbeckProcess(theta=0.15, mu=np.zeros(num_actions),
                                            sigma=float(NOISE_STD), sigma_min=0.1, n_steps_annealing=NOISE_ANNEALING_STEPS)

    scope = 'team_' + str(team) + '_ai_' + str(index)
    # i hope it's initialized under different scope!
    critic = Critic(scope+'/eval_net', layer_norm=True)
    actor = Actor(scope+'/eval_net', num_actions, layer_norm=True)

    player = DDPG(actor, critic, observation_shape, action_shape, Sess, scope=scope,
                parent_ai=None, action_noise=action_noise, gamma=0.99, tau=0.001, batch_size=BATCH_SIZE,
                action_range=action_range, critic_l2_reg=1e-2, actor_lr=1e-4, critic_lr=1e-3, clip_norm=None, reward_scale=0.01)

    player.build_memory(100000)
    return player

if __name__ == '__main__':
    config = tf.ConfigProto(allow_soft_placement=True, log_device_placement=False)
    config.gpu_options.allow_growth=True
    Sess = tf.Session(config=config)
    super_env = SuperEnv(n_players_every_side=PLAYERS_EVERY_SIDE, player_maker=player_maker, sess=Sess)

    super_env.start_game()
