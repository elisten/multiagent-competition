import time

SAVE_PATH = 'saved_models/' + time.ctime() + '/'
RESTORE = False
SAVE_EVERY = 50
SAVE_INTERVAL = 50

# ==== NOISE ======

NOISE_ANNEALING_STEPS = 1e6
NOISE_STD = 1

# ==== DATA =======
BATCH_SIZE = 300

# set it to one to debug
PLAYERS_EVERY_SIDE = 3

# ==== SAMPLE HERO =
HISTORY_LIMIT = 32

# ==== TRAIN =======
LEARN_STEP = 20

# ==== MOVE ANNELING =
MOVE_ANNEALING_STEPS = 1e6
