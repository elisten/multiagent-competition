# https://github.com/openai/baselines/baselines/ddpg/ddpg.py

from copy import copy
import tensorflow as tf
import numpy as np
import tensorflow.contrib as tc
from util import reduce_std
from config import SAVE_PATH
import os
from memory import Memory
import random
from math import ceil


def get_target_updates(vars, target_vars, tau):
    print('setting up target updates ...')
    soft_updates = []
    init_updates = []
    # for i in vars:
    #     print(i.name)
    # for i in target_vars:
    #     print(i.name)
    print(len(vars), len(target_vars))
    assert len(vars) == len(target_vars)
    for var, target_var in zip(vars, target_vars):
        init_updates.append(tf.assign(target_var, var))
        soft_updates.append(tf.assign(target_var, (1. - tau) * target_var + tau * var))
    assert len(init_updates) == len(vars)
    assert len(soft_updates) == len(vars)
    return tf.group(*init_updates), tf.group(*soft_updates)

# TODO initialize a new noise
class DDPG(object):
    def __init__(self, actor, critic, observation_shape, action_shape, sess, scope, parent_ai=None, action_noise=None,
                 gamma=0.99, tau=0.001, batch_size=32, action_range=(-1., 1.), critic_l2_reg=0,
                 actor_lr=1e-4, critic_lr=1e-3, clip_norm=None, reward_scale=1.):

        # Parameters.
        self.parent_ai = parent_ai
        self.gamma = gamma  # discount factor.
        self.tau = tau  # soft update
        self.action_noise = action_noise
        self.action_range = action_range
        self.actor_lr = actor_lr
        self.critic_lr = critic_lr
        self.clip_norm = clip_norm
        self.reward_scale = reward_scale
        self.batch_size = batch_size
        self.stats_sample = None
        self.critic_l2_reg = critic_l2_reg
        self.action_shape = action_shape
        self.observation_shape=  observation_shape
        self.scope = scope
        self.copies = 0
        self.learn_step = 0

        # Session
        self.sess = sess

        # train input
        self.obs0 = tf.placeholder(tf.float32, shape=(None,) + observation_shape, name='obs0')
        self.obs1 = tf.placeholder(tf.float32, shape=(None,) + observation_shape, name='obs1')
        self.terminals1 = tf.placeholder(tf.float32, shape=(None, 1), name='terminals1')
        self.rewards = tf.placeholder(tf.float32, shape=(None, 1), name='rewards')
        self.actions = tf.placeholder(tf.float32, shape=(None,) + action_shape, name='actions')

        self.critic_target = tf.placeholder(tf.float32, shape=(None, 1), name='critic_target')
        self.value_target = tf.placeholder(tf.float32, shape=(None, 1), name="value_target")

        self.actor_template = actor
        self.critic_template = critic

        self.critic = copy(critic)
        self.actor = copy(actor)

        target_actor = copy(actor)
        target_actor.scope = actor.scope.replace('eval_net', 'target_net') # build the network only when _call is invoked
        self.target_actor = target_actor

        target_critic = copy(critic)
        target_critic.scope = critic.scope.replace('eval_net', 'target_net')
        self.target_critic = target_critic  # a network or an object.

        with tf.variable_scope(self.scope):

            self._save_count = tf.get_variable(name='save_count', dtype=tf.int32, initializer=0)
            self.update_save_count = tf.assign(self._save_count, self._save_count + 1)

            with tf.variable_scope("eval_net"):
                self.actor_tf = actor(self.obs0)
                self.critic_tf, self.value_tf = critic(self.obs0, self.actions)
                self.critic_with_actor_tf, self.value_with_actor_tf = critic(self.obs0, self.actor_tf, reuse=True)

            with tf.variable_scope("target_net"):
                Q_obs1, V_obs1 = target_critic(self.obs1, target_actor(self.obs1))

        self.target_Q = self.rewards + (1. - self.terminals1) * gamma * Q_obs1
        self.target_V = self.rewards + (1. - self.terminals1) * gamma * V_obs1

        self.setup_actor_optimizer()
        self.setup_critic_optimizer()


        #  used to debug and follow the process.
        self.setup_stats()
        self.setup_target_network_updates()

        self.eval_params = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope=scope + '/eval_net')
        # self.target_params = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope=scope + '/target_net')
        self._make_save_dict()
        self.Saver = tf.train.Saver(self.save_dict, max_to_keep=None)


        print(self.scope,"has been constructed")
        if parent_ai is None:
            self.save_folder = SAVE_PATH + self.scope + '/'
            if not os.path.exists(self.save_folder):
                os.makedirs(self.save_folder)
        else:
            self._sync = [tf.assign(x, y) for x, y in zip(self.eval_params, parent_ai.eval_params)]
            self.parent_ai = parent_ai

    def sync(self):
        if self.learn_step != 0 and self.learn_step == self.parent_ai.learn_step: return self
        self.sess.run(self._sync)
        self.learn_step = self.parent_ai.learn_step
        return self

    # no needs for threads sampling the data
    def build_memory(self, memory_size):
        self.memory = Memory(limit=int(memory_size), action_shape=self.action_shape, observation_shape=self.observation_shape)


    def setup_target_network_updates(self):
        # init & soft
        actor_init_updates, actor_soft_updates = get_target_updates(self.actor.vars, self.target_actor.vars, self.tau)

        critic_init_updates, critic_soft_updates = get_target_updates(self.critic.vars, self.target_critic.vars,
                                                                      self.tau)

        self.target_init_updates = [actor_init_updates, critic_init_updates]
        self.target_soft_updates = [actor_soft_updates, critic_soft_updates]

    def setup_actor_optimizer(self):
        print("setting up actor optimizer")
        # because of reuse, no extra computation.
        # gradient ascent. use Q's direction to update. Thus. the gradient need to be small.
        # Maybe I should use advantage function.
        # How to update advantage function. ( update Q and V simultaneously )
        self.actor_loss = -tf.reduce_mean(self.critic_with_actor_tf - self.value_with_actor_tf)
        actor_shapes = [var.get_shape().as_list() for var in self.actor.trainable_vars]
        print('  actor shapes: {}'.format(actor_shapes))
        for var in self.actor.trainable_vars:
            print(var.name, "trainable")
        with tf.variable_scope(self.scope+'/actor_optimizer'):
            self.actor_optimizer = tf.train.AdamOptimizer(learning_rate=self.actor_lr,
                                                      beta1=0.9, beta2=0.999,epsilon=1e-8)
            actor_grads = tf.gradients(self.actor_loss, self.actor.trainable_vars)
            if self.clip_norm:
                self.actor_grads,_ = tf.clip_by_global_norm(actor_grads, self.clip_norm)
            else:
                self.actor_grads = actor_grads
            grads_and_vars = list(zip(self.actor_grads, self.actor.trainable_vars))
            self.actor_train_op = self.actor_optimizer.apply_gradients(grads_and_vars)
            

    # the update of value is almost independent of updating policy.
    def setup_critic_optimizer(self):
        print('setting up critic optimizer')
        # Most cases ( train ), off policy
        self.critic_loss = tf.reduce_mean(tf.square(self.critic_tf - self.critic_target))
        self.value_loss = tf.reduce_mean(tf.square(self.value_tf - self.value_target))
        self.critic_total_loss = self.critic_loss + self.value_loss
        if self.critic_l2_reg > 0.:
            critic_reg_vars = [var for var in self.critic.trainable_vars if
                               'kernel' in var.name and 'output' not in var.name]
            for var in critic_reg_vars:
                print('  regularizing: {}'.format(var.name))
            print('  applying l2 regularization with {}'.format(self.critic_l2_reg))
            critic_reg = tc.layers.apply_regularization(
                tc.layers.l2_regularizer(self.critic_l2_reg),
                weights_list=critic_reg_vars
            )
            self.critic_total_loss += critic_reg
        critic_shapes = [var.get_shape().as_list() for var in self.critic.trainable_vars]
        print('  critic shapes: {}'.format(critic_shapes))
        with tf.variable_scope(self.scope+'/critic_optimizer'):
            self.critic_optimizer = tf.train.AdamOptimizer(learning_rate=self.critic_lr,
                                                       beta1=0.9, beta2=0.999, epsilon=1e-8)
            critic_grads = tf.gradients(self.critic_total_loss, self.critic.trainable_vars)
            if self.clip_norm:
                self.critic_grads,_ = tf.clip_by_global_norm(critic_grads, self.clip_norm)
            else:
                self.critic_grads = critic_grads
            grads_and_vars = list(zip(self.critic_grads, self.critic.trainable_vars))
            self.critic_train_op = self.critic_optimizer.apply_gradients(grads_and_vars)

    def setup_stats(self):
        ops = []
        names = []

        # if self.normalize_returns:
        #     ops += [self.ret_rms.mean, self.ret_rms.std]
        #     names += ['ret_rms_mean', 'ret_rms_std']

        # if self.normalize_observations:
        #     ops += [tf.reduce_mean(self.obs_rms.mean), tf.reduce_mean(self.obs_rms.std)]
        #     names += ['obs_rms_mean', 'obs_rms_std']

        ops += [tf.reduce_mean(self.critic_tf)]
        names += ['reference_Q_mean']
        ops += [reduce_std(self.critic_tf)]
        names += ['reference_Q_std']

        ops += [tf.reduce_mean(self.critic_with_actor_tf)]
        names += ['reference_actor_Q_mean']
        ops += [reduce_std(self.critic_with_actor_tf)]
        names += ['reference_actor_Q_std']

        ops += [tf.reduce_mean(self.actor_tf)]
        names += ['reference_action_mean']
        ops += [reduce_std(self.actor_tf)]
        names += ['reference_action_std']

        self.stats_ops = ops
        self.stats_names = names

    def pi(self, obs, apply_noise=True, compute_Q=True):

        actor_tf = self.actor_tf
        feed_dict = {self.obs0: [obs]}
        if compute_Q:
            action, q = self.sess.run([actor_tf, self.critic_with_actor_tf], feed_dict=feed_dict)
        else:
            action = self.sess.run(actor_tf, feed_dict=feed_dict)
            q = None
        action = action.flatten()
        # I don't think the noise is adjusting to the action.
        if self.action_noise is not None and apply_noise:
            noise = self.action_noise()
            assert noise.shape == action.shape
            action += noise
            # noise is a value in each dimension
        #  Maybe I need a additional specification in this situation.
        # Continuous Space
        action = np.clip(action, self.action_range[0], self.action_range[1])
        return action, q

    def store_transition(self, obs0, action, reward, obs1, terminal1):
        reward *= self.reward_scale
        self.memory.append(obs0, action, reward, obs1, terminal1)
        # moving average.
        # if self.normalize_observations:
        #     self.obs_rms.update(np.array([obs0]))

    def train(self):
        self.learn_step += 1
        # Get a batch.
        batch = self.memory.sample(batch_size=self.batch_size)

        target_Q, target_V = self.sess.run([self.target_Q, self.target_V], feed_dict={
            self.obs1: batch['obs1'],
            self.rewards: batch['rewards'],
            self.terminals1: batch['terminals1'].astype('float32'),
        })

        # Get all gradients and perform a synced update.
        ops = [self.actor_train_op, self.actor_loss, self.critic_train_op, self.critic_total_loss]
        _, actor_loss, _, critic_loss = self.sess.run(ops, feed_dict={
            self.obs0: batch['obs0'],
            self.actions: batch['actions'],
            self.critic_target: target_Q,
            self.value_target: target_V
        })
        return critic_loss, actor_loss

    def initialize(self):
        # self.sess = sess
        # self.sess.run(tf.global_variables_initializer())
        self.sess.run(self.target_init_updates)

    def update_target_net(self):
        self.sess.run(self.target_soft_updates)

    def get_stats(self):
        if self.stats_sample is None:
            # Get a sample and keep that fixed for all further computations.
            # This allows us to estimate the change in value for the same set of inputs.
            self.stats_sample = self.memory.sample(batch_size=self.batch_size)
        values = self.sess.run(self.stats_ops, feed_dict={
            self.obs0: self.stats_sample['obs0'],
            self.actions: self.stats_sample['actions'],
        })

        names = self.stats_names[:]
        assert len(names) == len(values)
        stats = dict(zip(names, values))
        return stats

    def reset(self):
        # Reset internal state after an episode is complete.
        if self.action_noise is not None:
            self.action_noise.reset()

    @property
    def save_count(self):
        return self.sess.run(self._save_count)

    # exclude the save_count
    def sample_history_version(self, limit):
        save_count = self.parent_ai.save_count
        index = np.random.randint(max(save_count-limit,0), save_count + 1)
        if index is 0:
            return self.sync(),index
        else:
            self.Saver.restore(self.sess, self.parent_ai.save_folder + str(index) + '.ckpt')
            print('sampled history version {}'.format(index))
        return self, index

    # without noises.
    def act_only_copy(self):
        copy_scope = self.scope + '_copy_' + str(self.copies)
        self.actor_template.scope = copy_scope + '/eval_net'
        self.critic_template.scope = copy_scope + '/eval_net'

        player_copy = DDPG(self.actor_template, self.critic_template, self.observation_shape, self.action_shape, self.sess,
                           copy_scope ,parent_ai=self, action_noise=self.action_noise,
                           gamma=self.gamma, tau=self.tau, batch_size=self.batch_size,
                           action_range=self.action_range, critic_l2_reg=self.critic_l2_reg,
                            actor_lr=self.actor_lr, critic_lr=self.critic_lr, clip_norm=self.clip_norm, reward_scale=self.reward_scale)
        self.copies += 1
        return player_copy

    def save(self):
        self.sess.run(self.update_save_count)
        self.Saver.save(self.sess, self.save_folder + str(self.save_count) + '.ckpt')

    def restore(self, restore_path):
        self.Saver.restore(restore_path)

    def _make_save_dict(self):
        self.save_dict = {}
        for v in self.eval_params:
            self.save_dict[v.name[v.name.find('/') + 1:]] = v

        # for v in self.target_params:
        #     self.save_dict[v.name[v.name.find('/') + 1:]] = v
