import sys

sys.path.append('../')

from config import *
import tensorflow as tf
import threading as td
import time
import numpy as np
import pickle
import gym
import gym_compete
from Defines import TEAMS

# Modify to the simple version.
class SuperEnv():
    def __init__(self, n_players_every_side, player_maker, sess, env_type="you-shall-not-pass-humans-v0"):
        # ======== initialization =======
        self.env_type = env_type
        self.env = gym.make(env_type)
        self.n_players_every_side = n_players_every_side
        self.sess = sess
        # winrate compared to different opponents.
        self.record = [[[] for i in range(n_players_every_side)] for j in range(n_players_every_side)]
        # global networok; use i to note the camp; use j to note the id.
        print("Crashed in making...")
        self.players_global = [[player_maker(sess, i, j, self.env) for j in range(n_players_every_side)] for i in range(2)]

        # all the enemy
        self.players_local = [[[self.players_global[1-i][j].act_only_copy() for j in range(n_players_every_side)]
                               for _ in range(n_players_every_side)] for i in range(2)]
        self.record = [[[[0,0,0] for _ in range(n_players_every_side)] for j in range(n_players_every_side)]
                                                                for i in range(2)]

        self.learn_steps = 0

        # TODO use multiple threads to sample data
        sess.run(tf.global_variables_initializer())
        for i in range(2):
            for j in range(n_players_every_side):
                self.players_global[i][j].initialize()

    def start_game(self):
        # self.players_local[i][j] is the list of opponents it can choose from.
        for i in range(2):
            for j in range(self.n_players_every_side):
                new_thread = td.Thread(target=self._play_and_learn, args=(self.players_global[i][j],
                                                                          self.players_local[i][j], i, j))
                new_thread.start()

        while True:
            time.sleep(1)
            if self.learn_steps % (SAVE_INTERVAL * self.n_players_every_side) == 0:
                file = open(SAVE_PATH + str(self.learn_steps) + 'win_loss_records', 'wb')
                pickle.dump(self.record, file)
                file.close()


    def _play_and_learn(self, learn_ai, opponent_ai_list, train_team, train_id):
        env = gym.make(self.env_type)
        decre = 1.0/MOVE_ANNEALING_STEPS
        move_reward_weight = 1.0
        num_episodes = 0
        step  = last_step = 0
        while True:
            num_episodes += 1
            frame_counter = 0
            opponent_id = np.random.randint(0, self.n_players_every_side)
            opponent_ai, history_id = opponent_ai_list[opponent_id].sample_history_version(HISTORY_LIMIT)

            print(" %s-%i-%i: %i VS %s-%i-%i: %i" % (TEAMS[train_team], train_id, learn_ai.save_count, self.record[train_team][train_id][opponent_id][0],
								TEAMS[1-train_team], opponent_id, history_id,self.record[train_team][train_id][opponent_id][1] ))

            obs = env.reset()
            episode_reward = 0

            while True:
                frame_counter += 1

                learn_action, learn_q = learn_ai.pi(obs[train_team], apply_noise=True, compute_Q=True)
                opponent_action,opponent_q = opponent_ai.pi(obs[1-train_team], apply_noise=False, compute_Q=False)

                if train_team == 0:
                    action = tuple([learn_action, opponent_action])
                else:
                    action = tuple([opponent_action, learn_action])

                next_obs, reward, done, infos = env.step(action)
                learn_reward = infos[train_team]['reward_move']*move_reward_weight + infos[train_team]['reward_remaining']*(1-move_reward_weight)
                move_reward_weight = max(move_reward_weight - decre, 0)
                learn_ai.store_transition(obs[train_team], learn_action, learn_reward, next_obs[train_team], done[0])
                obs = next_obs

                episode_reward += reward[train_team]

                if done[0]:
                    learn_ai.reset()
                    obs = env.reset()
                    draw = True
                    for i in range(2):
                        if 'winner' in infos[i]:
                            draw = False
                            if i == train_team:
                                # win
                                self.record[train_team][train_id][opponent_id][0] += 1
                            else:
                                self.record[train_team][train_id][opponent_id][1] += 1
                    if draw:
                        self.record[train_team][train_id][opponent_id][2] += 1

                    for _ in range(LEARN_STEP):
                        learn_ai.train()
                    print("Episode Reward: %f" % episode_reward)
                    episode_reward = 0
                    self.learn_steps += 1
                    step += 1
                    if step - last_step >= SAVE_INTERVAL:
                        learn_ai.save()
                        last_step = step
                    break
